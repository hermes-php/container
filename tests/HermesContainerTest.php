<?php

/*
 * This file is part of the Hermes\Container library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\Container\Tests;

use Hermes\Container\HermesContainer;
use Hermes\Container\Tests\Builder\StubServiceOne;
use Hermes\Container\Tests\Builder\StubServiceThree;
use Hermes\Container\Tests\Builder\StubServiceTwo;
use PHPUnit\Framework\TestCase;

/**
 * Class HermesContainerTest.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class HermesContainerTest extends TestCase
{
    /**
     * @var HermesContainer
     */
    private $container;

    public function setUp()
    {
        $this->container = new HermesContainer([
            StubServiceOne::class => function ($container) { return new StubServiceOne($container->get(StubServiceTwo::class)); },
            StubServiceTwo::class => StubServiceTwo::class,
            StubServiceThree::class => StubServiceThree::class,
        ]);
    }

    public function testThatServicesWereRegisteredCorrectly(): void
    {
        $this->assertInstanceOf(StubServiceOne::class, $this->container->get(StubServiceOne::class));
    }
}
