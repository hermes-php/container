<?php

/*
 * This file is part of the Hermes\Container library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\Container\Tests\Builder;

/**
 * Class StubServiceTwo.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class StubServiceTwo
{
    /**
     * @var StubServiceThree
     */
    private $serviceThree;

    /**
     * StubServiceTwo constructor.
     *
     * @param StubServiceThree $serviceThree
     */
    public function __construct(StubServiceThree $serviceThree)
    {
        $this->serviceThree = $serviceThree;
    }

    /**
     * @return StubServiceThree
     */
    public function getServiceThree(): StubServiceThree
    {
        return $this->serviceThree;
    }
}
