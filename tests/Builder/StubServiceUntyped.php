<?php

/*
 * This file is part of the Hermes\Container library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\Container\Tests\Builder;

/**
 * Class StubServiceUntyped.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class StubServiceUntyped
{
    private $hello;

    public function __construct($hello)
    {
        $this->hello = $hello;
    }
}
