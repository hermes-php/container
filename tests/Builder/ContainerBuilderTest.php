<?php

/*
 * This file is part of the Hermes\Container library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\Container\Tests\Builder;

use Hermes\Container\Builder\ContainerBuilder;
use Hermes\Container\Builder\Reference;
use Hermes\Container\ContainerException;
use Hermes\Container\HermesContainer;
use PHPUnit\Framework\TestCase;

/**
 * Class ContainerBuilderTest.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class ContainerBuilderTest extends TestCase
{
    public function testThatServicesAreAutowired(): void
    {
        $builder = new ContainerBuilder();
        $builder->class(StubServiceOne::class, StubServiceOne::class)->autowire();
        $builder->class(StubServiceTwo::class, StubServiceTwo::class)->autowire();
        $builder->class(StubServiceThree::class, StubServiceThree::class)->autowire();

        $container = HermesContainer::fromBuilder($builder);

        $this->assertTrue($container->has(StubServiceOne::class));
        $this->assertTrue($container->has(StubServiceTwo::class));
        $this->assertTrue($container->has(StubServiceThree::class));

        $this->assertInstanceOf(StubServiceOne::class, $container->get(StubServiceOne::class));
    }

    public function testSingleton(): void
    {
        $builder = new ContainerBuilder();
        $builder->class(StubServiceOne::class, StubServiceOne::class)->autowire();
        $builder->class(StubServiceTwo::class, StubServiceTwo::class)->autowire();
        $builder->class(StubServiceThree::class, StubServiceThree::class)->autowire();

        $container = HermesContainer::fromBuilder($builder);

        $hashOne = spl_object_hash($container->get(StubServiceOne::class));
        $hashTwo = spl_object_hash($container->get(StubServiceOne::class));

        $this->assertSame($hashOne, $hashTwo);

        $serviceOne = $container->get(StubServiceOne::class);
        $serviceOne->touch();

        $this->assertTrue($serviceOne->isChanged());

        $serviceTwo = $container->get(StubServiceOne::class);
        $this->assertTrue($serviceTwo->isChanged());
    }

    public function testNoSingleton(): void
    {
        $builder = new ContainerBuilder();
        $builder->class(StubServiceOne::class, StubServiceOne::class)->autowire()->noSingleton();
        $builder->class(StubServiceTwo::class, StubServiceTwo::class)->autowire();
        $builder->class(StubServiceThree::class, StubServiceThree::class)->autowire();

        $container = HermesContainer::fromBuilder($builder);

        $serviceOne = $container->get(StubServiceOne::class);
        $serviceOne->touch();

        $this->assertTrue($serviceOne->isChanged());

        $serviceTwo = $container->get(StubServiceOne::class);
        $this->assertFalse($serviceTwo->isChanged());
    }

    public function testAutowireFails(): void
    {
        $builder = new ContainerBuilder();
        $builder->class(StubServiceOne::class, StubServiceOne::class)->autowire();
        $builder->class(StubServiceTwo::class, StubServiceTwo::class)->autowire();

        $container = HermesContainer::fromBuilder($builder);

        $this->expectException(ContainerException::class);
        $container->get(StubServiceOne::class);
    }

    public function testAutowiringFailsOnBuiltInParam(): void
    {
        $builder = new ContainerBuilder();
        $builder->class(StubNoParam::class, StubNoParam::class)->autowire();

        $container = HermesContainer::fromBuilder($builder);

        $this->expectException(ContainerException::class);
        $container->get(StubNoParam::class);
    }

    public function testAutowiringFailsOnUntypedParam(): void
    {
        $builder = new ContainerBuilder();
        $builder->class(StubServiceUntyped::class, StubServiceUntyped::class)->autowire();

        $container = HermesContainer::fromBuilder($builder);

        $this->expectException(ContainerException::class);
        $container->get(StubServiceUntyped::class);
    }

    public function testFactory(): void
    {
        $builder = new ContainerBuilder();
        $builder->factory(StubServiceOne::class, function () {
            return new StubServiceOne(new StubServiceTwo(new StubServiceThree()));
        });

        $container = HermesContainer::fromBuilder($builder);

        $this->assertTrue($container->has(StubServiceOne::class));

        $this->assertInstanceOf(StubServiceOne::class, $container->get(StubServiceOne::class));
    }

    public function testAliasIsRegistered(): void
    {
        $builder = new ContainerBuilder();
        $builder->factory(StubServiceOne::class, function () {
            return new StubServiceOne(new StubServiceTwo(new StubServiceThree()));
        });

        $builder->alias(StubServiceOne::class, 'stubServiceOne');

        $container = HermesContainer::fromBuilder($builder);

        $this->assertTrue($container->has(StubServiceOne::class));
        $this->assertTrue($container->has('stubServiceOne'));

        $service = $container->get(StubServiceOne::class);
        $this->assertFalse($service->isChanged());
        $service->touch();
        $this->assertTrue($service->isChanged());

        $alias = $container->get('stubServiceOne');

        $this->assertInstanceOf(StubServiceOne::class, $alias);
        $this->assertTrue($alias->isChanged());
    }

    public function testReferenceClass(): void
    {
        $builder = new ContainerBuilder();
        $builder->factory(StubServiceOne::class, function () {
            return new StubServiceOne(new StubServiceTwo(new StubServiceThree()));
        });

        $builder->class(StubServiceWithSetter::class, StubServiceWithSetter::class)
            ->addMethodCall('setStubOne', new Reference(StubServiceOne::class));

        $container = HermesContainer::fromBuilder($builder);

        $this->assertTrue($container->has(StubServiceOne::class));
        $this->assertTrue($container->has(StubServiceWithSetter::class));

        $this->assertInstanceOf(StubServiceOne::class, $container->get(StubServiceOne::class));
    }

    public function testNoReference(): void
    {
        $builder = new ContainerBuilder();

        $builder->class(StubServiceWithSetter::class, StubServiceWithSetter::class)
            ->addMethodCall('setStubOne', new StubServiceOne(new StubServiceTwo(new StubServiceThree())));

        $container = HermesContainer::fromBuilder($builder);

        $this->assertTrue($container->has(StubServiceWithSetter::class));
    }
}
