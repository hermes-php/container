<?php

/*
 * This file is part of the Hermes\Container library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\Container\Tests\Builder;

/**
 * Class StubServiceThree.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class StubServiceThree
{
}
