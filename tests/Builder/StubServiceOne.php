<?php

/*
 * This file is part of the Hermes\Container library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\Container\Tests\Builder;

/**
 * Class StubServiceOne.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class StubServiceOne
{
    /**
     * @var StubServiceTwo
     */
    private $serviceTwo;
    /**
     * @var bool
     */
    private $isChanged = false;

    public function __construct(StubServiceTwo $serviceTwo)
    {
        $this->serviceTwo = $serviceTwo;
    }

    /**
     * @return StubServiceTwo
     */
    public function getServiceTwo(): StubServiceTwo
    {
        return $this->serviceTwo;
    }

    public function touch(): void
    {
        $this->isChanged = true;
    }

    /**
     * @return bool
     */
    public function isChanged(): bool
    {
        return $this->isChanged;
    }
}
