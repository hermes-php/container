<?php

/*
 * This file is part of the Hermes\Container library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\Container;

use Psr\Container\NotFoundExceptionInterface;

/**
 * Class ServiceNotFoundException.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class ServiceNotFoundException extends ContainerException implements NotFoundExceptionInterface
{
    public function __construct(string $id)
    {
        parent::__construct(sprintf('Service %s does not exist', $id));
    }
}
