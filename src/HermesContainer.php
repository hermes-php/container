<?php

/*
 * This file is part of the Hermes\Container library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\Container;

use Hermes\Container\Builder\ClassDefinition;
use Hermes\Container\Builder\ContainerBuilder;
use Hermes\Container\Builder\FactoryDefinition;
use Psr\Container\ContainerInterface;

/**
 * Class HermesContainer.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class HermesContainer implements ContainerInterface
{
    /**
     * @var object|callable[]
     */
    private $services;

    /**
     * This constructor is intended for simple configuration. It only accepts
     * fully-qualified class names strings or callables.
     *
     * It converts the first in a autowired class definition.
     *
     * The second one is transformed into a factory definition.
     *
     * @param callable[]|string[] $services
     */
    public function __construct(array $services = [])
    {
        foreach ($services as $id => $service) {
            if (\is_callable($service)) {
                $this->services[$id] = new FactoryDefinition($service);
                continue;
            }
            if (\is_string($service) && \class_exists($service)) {
                $this->services[$id] = (new ClassDefinition($service))->autowire();
                continue;
            }
            $this->services[$id] = $service;
        }
    }

    /**
     * @param ContainerBuilder $builder
     *
     * @return HermesContainer
     */
    public static function fromBuilder(ContainerBuilder $builder): HermesContainer
    {
        $self = new self();
        $self->services = $builder->getDefinitions();

        return $self;
    }

    /**
     * @param string $id
     *
     * @return callable|mixed
     *
     * @throws ServiceNotFoundException
     * @throws ContainerException
     */
    public function get($id)
    {
        if (!$this->has($id)) {
            throw new ServiceNotFoundException($id);
        }

        if (\is_callable($this->services[$id])) {
            return $this->services[$id]($this);
        }

        return $this->services[$id];
    }

    public function has($id): bool
    {
        return array_key_exists($id, $this->services);
    }
}
