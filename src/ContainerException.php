<?php

/*
 * This file is part of the Hermes\Container library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\Container;

use Psr\Container\ContainerExceptionInterface;

/**
 * Class ContainerException.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class ContainerException extends \RuntimeException implements ContainerExceptionInterface
{
    /**
     * @param string $paramName
     * @param string $classname
     *
     * @return ContainerException
     */
    public static function requiredParameterIsNotTyped(string $classname, string $paramName): ContainerException
    {
        return new self(sprintf('Cannot autowire %s class. Parameter "%s" is not typed and is required.', $classname, $paramName));
    }

    /**
     * @param string $classname
     * @param string $paramName
     * @param string $type
     *
     * @return ContainerException
     */
    public static function parameterTypeIsBuiltIn(string $classname, string $paramName, string $type): ContainerException
    {
        return new self(sprintf('Cannot autowire %s class. Parameter "%s" is of built-in type "%s".', $classname, $paramName, $type));
    }

    /**
     * @param string $classname
     * @param string $paramName
     * @param string $type
     *
     * @return ContainerException
     */
    public static function parameterTypeNameNotFound(string $classname, string $paramName, string $type): ContainerException
    {
        return new self(sprintf('Cannot autowire %s class. Service typed as "%s" for parameter "%s" does not exist in container.', $classname, $type, $paramName));
    }
}
