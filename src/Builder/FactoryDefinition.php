<?php

/*
 * This file is part of the Hermes\Container library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\Container\Builder;

use Psr\Container\ContainerInterface;

/**
 * Wraps a callable factory to provide some config.
 *
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
final class FactoryDefinition extends BaseDefinition
{
    /**
     * @var callable
     */
    private $callable;

    /**
     * FactoryDefinition constructor.
     *
     * @param callable $callable
     */
    public function __construct(callable $callable)
    {
        $this->callable = $callable;
    }

    /**
     * @param ContainerInterface $container
     *
     * @return object|null
     */
    public function __invoke(ContainerInterface $container)
    {
        if (false === $this->singleton) {
            return $this->resolveCallable($container);
        }
        if (null === $this->state) {
            $this->state = $this->resolveCallable($container);
        }

        return $this->state;
    }

    /**
     * @return $this
     */
    public function noSingleton(): FactoryDefinition
    {
        $this->singleton = false;

        return $this;
    }

    /**
     * @param ContainerInterface $container
     *
     * @return mixed
     */
    private function resolveCallable(ContainerInterface $container)
    {
        return ($this->callable)($container);
    }
}
