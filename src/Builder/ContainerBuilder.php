<?php

/*
 * This file is part of the Hermes\Container library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\Container\Builder;

/**
 * Class ContainerBuilder.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class ContainerBuilder
{
    /**
     * @var callable[]
     */
    private $definitions;

    /**
     * Registers a callable that returns an instance of the service..
     *
     * @param string   $serviceId
     * @param callable $factory
     *
     * @return FactoryDefinition
     */
    public function factory(string $serviceId, callable $factory): FactoryDefinition
    {
        $definition = new FactoryDefinition($factory);
        $this->definitions[$serviceId] = $definition;

        return $definition;
    }

    /**
     * @param string   $serviceId
     * @param string   $classname
     * @param string[] $dependencies
     *
     * @return ClassDefinition
     */
    public function class(string $serviceId, string $classname, string ...$dependencies): ClassDefinition
    {
        $definition = new ClassDefinition($classname, ...$dependencies);
        $this->definitions[$serviceId] = $definition;

        return $definition;
    }

    /**
     * @param string $existingService
     * @param string $aliasName
     */
    public function alias(string $existingService, string $aliasName): void
    {
        $this->definitions[$aliasName] = &$this->definitions[$existingService];
    }

    /**
     * @return array
     */
    public function getDefinitions(): array
    {
        return $this->definitions;
    }
}
