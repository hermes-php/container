<?php

/*
 * This file is part of the Hermes\Container library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\Container\Builder;

use Hermes\Container\ContainerException;
use Psr\Container\ContainerInterface;

/**
 * Class ClassDefinition.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
final class ClassDefinition extends BaseDefinition
{
    /**
     * @var string
     */
    private $classname;
    /**
     * @var bool
     */
    private $autowire = false;
    /**
     * @var array
     */
    private $methodCalls = [];
    /**
     * @var array|string[]
     */
    private $dependencies = [];

    /**
     * ClassDefinition constructor.
     *
     * @param string   $classname
     * @param string[] $dependencies
     */
    public function __construct(string $classname)
    {
        $this->classname = $classname;
    }

    /**
     * @param ContainerInterface $container
     *
     * @return mixed|object|null
     *
     * @throws \ReflectionException
     */
    public function __invoke(ContainerInterface $container)
    {
        if (!\class_exists($this->classname)) {
            throw new \RuntimeException(sprintf('Cannot instantiate %s class. It does not exist.', $this->classname));
        }
        if (false === $this->singleton) {
            return $this->createInstance($container);
        }
        if (null === $this->state) {
            $this->state = $this->createInstance($container);
        }

        return $this->state;
    }

    /**
     * @return ClassDefinition
     */
    public function autowire(): ClassDefinition
    {
        $this->autowire = true;

        return $this;
    }

    /**
     * @return ClassDefinition
     */
    public function noSingleton(): ClassDefinition
    {
        $this->singleton = false;

        return $this;
    }

    /**
     * @param string $methodName
     * @param mixed  ...$arguments
     *
     * @return ClassDefinition
     */
    public function addMethodCall(string $methodName, ...$arguments): ClassDefinition
    {
        $this->methodCalls[$methodName] = $arguments;

        return $this;
    }

    /**
     * @param mixed ...$arguments
     *
     * @return ClassDefinition
     */
    public function setConstructorArguments(...$arguments): ClassDefinition
    {
        $this->dependencies = $arguments;

        return $this;
    }

    /**
     * @param ContainerInterface $container
     *
     * @return mixed
     *
     * @throws \ReflectionException
     */
    private function createInstance(ContainerInterface $container)
    {
        if (true === $this->autowire) {
            $dependencies = $this->guessDependencies($container);
        } else {
            $dependencies = $this->getDependencies($container);
        }

        $instance = new $this->classname(...$dependencies);

        foreach ($this->methodCalls as $method => $arguments) {
            $newArgs = [];
            foreach ($arguments as $argument) {
                if ($argument instanceof Reference) {
                    $newArgs[] = $argument->resolve($container);
                } else {
                    $newArgs[] = $argument;
                }
            }
            $instance->{$method}(...$newArgs);
        }

        return $instance;
    }

    /**
     * @param ContainerInterface $container
     *
     * @return array
     *
     * @throws \ReflectionException
     */
    private function guessDependencies(ContainerInterface $container): array
    {
        $instances = [];
        $constructor = (new \ReflectionClass($this->classname))->getConstructor();
        if (null === $constructor) {
            return [];
        }
        $parameters = $constructor->getParameters();
        foreach ($parameters as $param) {
            if (!$param->hasType() && !$param->isOptional()) {
                throw ContainerException::requiredParameterIsNotTyped($this->classname, $param->getName());
            }
            if ($param->getType()->isBuiltin()) {
                throw ContainerException::parameterTypeIsBuiltIn($this->classname, $param->getName(), $param->getType()->getName());
            }
            if (!$container->has($param->getType()->getName())) {
                throw ContainerException::parameterTypeNameNotFound($this->classname, $param->getName(), $param->getType()->getName());
            }
            $instances[] = $container->get($param->getType()->getName());
        }

        return $instances;
    }

    /**
     * @param ContainerInterface $container
     *
     * @return array
     */
    private function getDependencies(ContainerInterface $container): array
    {
        $instances = [];
        foreach ($this->dependencies as $dependency) {
            if ($dependency instanceof Reference) {
                $instances[] = $dependency->resolve($container);
            } else {
                $instances[] = $dependency;
            }
        }

        return $instances;
    }
}
