<?php

/*
 * This file is part of the Hermes\Container library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\Container\Builder;

/**
 * Class BaseDefinition.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
abstract class BaseDefinition
{
    /**
     * The state of the service.
     *
     * @var object|null
     */
    protected $state;
    /**
     * @var bool
     */
    protected $singleton = true;
}
