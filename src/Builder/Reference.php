<?php

/*
 * This file is part of the Hermes\Container library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\Container\Builder;

use Psr\Container\ContainerInterface;

/**
 * Class Reference.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
final class Reference
{
    /**
     * @var string
     */
    private $serviceId;

    /**
     * Reference constructor.
     *
     * @param string $serviceId
     */
    public function __construct(string $serviceId)
    {
        $this->serviceId = $serviceId;
    }

    /**
     * @param ContainerInterface $container
     *
     * @return mixed
     */
    public function resolve(ContainerInterface $container)
    {
        return $container->get($this->serviceId);
    }
}
