Hermes Container
================

A simple PSR-11 DI Container

## Installation

```bash
$ composer require hermes-php/container
```

## Usage

Hermes Container is a really simple and minimal container. It is specially tailored
for situations when you don't need really complex stuff. It covers most of the 
use cases for DI Containers.

The easiest way to create a container is by passing it an array of services:

```php
<?php

use MyAwesomeApp\Services;
use MyAwesomeApp\Factories;
use Psr\Container\ContainerInterface;

$services = [
    // You can pass any kind of callable, like invokable service factories
    Services\ServiceOne::class => new Factories\ServiceOneFactory(),
    // You can pass an anonymous functions directly too
    Services\ServiceTwo::class => function(ContainerInterface $container) {
        return new Services\ServiceTwo();   
    },
    // When you don't need complex instantiation logic, you can register a simple class name
    // Hermes Container will try to autowire it based on its constructor dependencies.
    Services\ServiceThreeInterface::class => Services\ServiceThreeConcrete::class
];

// Then, you pass all this to the Hermes Container constructor
$container = new \Hermes\Container\HermesContainer($services);

// Then you can fetch its dependencies by the service name.
$container->get(Services\ServiceThreeInterface::class);
```

## The Container Builder
For cases when just a simple array config is not enough, you can make use of the
container builder. This is a service that allows you to register services with a
little bit more of configuration and control.

You can even "pass" this builder to, for example, plugins or modules in your application
so they can register their services in it.

```php
<?php

use Hermes\Container\Builder\ContainerBuilder;
use MyAwesomeApp\Services;
use MyAwesomeApp\Factories;
use Hermes\Container\Builder\Reference;
use Hermes\Container\HermesContainer;

$builder = new ContainerBuilder();

// Lets start with a factory
$builder->factory(Services\ServiceOne::class, new Factories\ServiceOneFactory())
    // This will give you a different instance every time the service is fetched
    ->noSingleton();

// And now a class.
$builder->class(Services\ServiceThreeInterface::class, Services\ServiceThreeConcrete::class)
    // We want to auto wire it's constructor dependencies
    ->autowire() 
    // And we want to call another setter method with a reference to another service.
    ->addMethodCall('setLogger', new Reference(\Psr\Log\LoggerInterface::class))
    // You can also make this a non-singleton
    ->noSingleton()
    // If you don't like autowiring (because reflection is expensive), you can be
    // explicit about the constructor arguments.
    ->setConstructorArguments(new Reference(Services\SomeService::class), new Reference(Services\AnotherService::class));

// Finally, you can create an alias for a service
$builder->alias(Services\ServiceThreeInterface::class, 'serviceThree');

// When you are done, you can build the Container from the Builder instance.
$container = HermesContainer::fromBuilder($builder);
```

That's pretty much about it!